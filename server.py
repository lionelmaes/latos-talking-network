from flask import Flask
from flask_socketio import SocketIO
import json
import _globals
import config
import logging
from logger import logger

import time

class Server:
    def __init__(self):
        self.thread = None
        ##INIT
        self.app = Flask(__name__)
        log = logging.getLogger('werkzeug')
        log.disabled = True
        self.running = False

        self.sio = SocketIO(self.app, logger = False)

        #here we are going to store the authorizations given by the clients
        self.clockAuth = []
        self.clockAuthSent = False




        @self.sio.on("connect")
        def connect():
            logger.debug("the server got a connection")
            
        @self.sio.on("disconnect")
        def disconnect():
            
            if _globals.restarting:
                logger.debug("already restarting")
                return
            _globals.restarting = True
            logger.debug('a client just disconnected')
            _globals.internalState = 0
            

        @self.sio.on("node_connection")
        def node_connection(data):
            with _globals.lock:
                _globals.talker.talk([data['ip'], "matrouve"])     

        @self.sio.on("message")
        def message(data):
            print("message ", data)

        @self.sio.on("ask_server_state")
        def send_server_state():
            self.sio.emit("server_state", _globals.internalState)

        @self.sio.on("ask_server_clockwill")
        def send_server_clockwill(data):
            logger.debug(f"{data['ip']}me demande si je veux être l'horloge")
            with _globals.lock:
                if _globals.clockWill:
                    _globals.talker.talk([data['ip'], "ouijeveuxetrelhorloge"])
                else:
                     _globals.talker.talk([data['ip'], "nonjeneveuxpasetrelhorloge"])

            self.sio.emit("server_clockwill", _globals.clockWill)

        @self.sio.on("clock_authorization")
        def receive_clock_authorization(data):
            logger.debug(f"je reçois l'authorisation de {data['ip']}: {data['auth']}")
            self.clockAuth.append({'ip':data['ip'], 'auth':data['auth']})





    def start_server(self):
        
        logger.debug('starting server')
        
        self.sio.run(self.app, host='0.0.0.0', port=config.PORT, debug=False, use_reloader=False)
        self.running = True

    def start(self):
        self.thread = self.sio.start_background_task(self.start_server)

    def stop(self):
        
        logger.debug("on essaye d'arrêter ce **** de serveur")
        self.sio.stop()
        
        
        
            
        
    def send_msg(self, type, data):
       
        logger.debug(f"send_msg: {type}")
        self.sio.emit(type, data)
    
    
    def wait(self):
        self.thread.join()





