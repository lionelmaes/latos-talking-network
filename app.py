from gevent import monkey
monkey.patch_all()
from logging import NullHandler
from socket import *
from netaddr import *
import netifaces as ni
import random
import subprocess
import datetime
from logger import logger
from server import Server
from client import Client
from talker import Talker
import time 
import _globals
import config
import alsaaudio
import threading
import datetime

#########INIT
def getHostNameIP(): 
    hostName = gethostname()
    hostIP = ni.ifaddresses('wlan0')[ni.AF_INET][0]['addr']
    return {'hostname':hostName, 'ip':hostIP}


########STATE 0: ip scan
def getServerFromIP(ip):
    global clients
    for client in clients:
        if client.ip == ip:
            return True
    return False


def lookForServers():
    global clients
    iprange = IPRange(config.IP_START, config.IP_END)
    
    #update client list with their status
    clients = [c for c in clients if c.status == 'up']

    #remove up servers ips + self ip from iprange list

    ipsToScan = [ip for ip in iprange if not getServerFromIP(ip) and str(ip) != _globals.ip]
    #ipsToScan = ["10.42.0.100", "10.42.0.100"]
    #look up for servers
    for ip in ipsToScan:
        
        newClient = Client(ip, config.PORT)
        if(newClient.connect()):
           
            clients.append(newClient)
    
    if len(ipsToScan) == 0:
        return True
    return False


clockWillState = -1
###############STATE 3 choose clock
def resetClockState():

    global clockWillState
    global clients 
    
    global clock
    global isClock

    clock = None
    isClock = False
    clockWillState = -1
    
    _globals.server.clockAuth = []
    _globals.server.clockAuthSent = False

    for c in clients:
        c.clockWill = None
        c.clockAuthorization = None
        c.clockAuths = None



def clockState():
    global clockWillState
    global clients 
    
    global clock
    global isClock
    #déclaration d'intention
    logger.debug(f"clockWillState: {clockWillState}")
    if clockWillState == -1:
        
        with _globals.lock:
                if _globals.clockWill:
                    _globals.talker.talk(['jeveuxetrelhorloge'])
                else:
                    
                    _globals.talker.talk(['jeneveuxpasetrelhorloge'])
                    

        clockWillState = 0

    elif clockWillState == 0:
        clockWillState = 1#on s'assure qu'on ne repassera plus par ici à ce tour ci

        ###on demande à chaque serveur sa volonté d'être l'horloge
        for c in clients:
            c.askServerClockWill()
        

    elif clockWillState == 1:
        ##on s'assure que chaque serveur a répondu
        clocksWillDefined = True
        for c in clients:
            if c.clockWill == None:
                clocksWillDefined = False
        
        logger.debug(f"nbre d autorisations recues: {len(_globals.server.clockAuth)}")
        ##on check si le node a reçu les authorisations des autres nodes
        if clocksWillDefined and len(_globals.server.clockAuth) == len(clients):
            ##si oui, on envoie ses authorisations à tout le monde
           
            _globals.server.send_msg('server_clock_auths', _globals.server.clockAuth)
            clockWillState = 2
        
    elif clockWillState == 2:
        ##est-ce que tous les clients ont bien reçu la table d'authorisation de leur serveur?
        
        for c in clients:
            if c.clockAuths == None:
                return

        
        #ici on a pour chaque client une table d'authorisation
        
        candidates = []

        #first the current node as a candidate if it wants it
        if _globals.clockWill:
            candidate = {'ip':_globals.ip, 'points':0}
            
            for response in _globals.server.clockAuth:
                if response['auth'] == True:
                    candidate['points'] += 1
            
            candidates.append(candidate)

        for c in clients:
            if c.clockWill == False:
                continue
            candidate = {'ip':c.ip, 'points':0, 'client':c}
            for response in c.clockAuths:
                print(response)
                if response['auth'] == True:
                    candidate['points'] += 1
            candidates.append(candidate)

        
        logger.info(f"#############attention voilà les candidats#################\n{[str(c) for c in candidates]}")

        if len(candidates) == 0:
            
            with _globals.lock:
                _globals.talker.talk(["personneneveutetrelhorloge", "recommencons"])

                _globals.internalState = 2
            
            return
        
        elected = None
        maxPoints = 0
        for candidate in candidates:
            if candidate['points'] > maxPoints:
                elected = candidate
                maxPoints = candidate['points']
            elif candidate['points'] == maxPoints:
                elected = None

        if elected == None:
            if _globals.clockWill:
                _globals.frustration += 1
            with _globals.lock:
                _globals.talker.talk(["personnenepeutetrelhorloge", "recommencons"])
                _globals.internalState = 2

                return
        
        
        if elected["ip"] == _globals.ip:
            isClock = True
            _globals.frustration = 0
            time.sleep(5)

            with _globals.lock:
              
                _globals.talker.talk(["vousmacceptezcommehorloge", "jesuislhorloge"])
        else:
            if _globals.clockWill:
                _globals.frustration += 1
            

            if _globals.frustration >= config.MAX_FRUSTRATION:
                logger.debug(f"that's it I'm out")
                _globals.server.sio.emit("server_stop")
                
                _globals.server.stop()
                _globals.restarting = True
                with _globals.lock:
                    _globals.talker.talk(["celafait", _globals.frustration, "fois", "quevousnemacceptezpascommehorloge"])
                    #_globals.talker.talk(["jenacceptepasque", elected["ip"], "soitlhorloge"])
                    _globals.talker.talk(["jeredemarre"])
                
                _globals.frustration = 0
                _globals.internalState = 0
                
                return
            
            clock = elected["client"]
            for c in clients:
                if c != clock:
                    c.clockHistory = 0
            clock.clockHistory += 1

            with _globals.lock:
                
                _globals.talker.talk(["nousacceptons", elected["ip"], "commehorloge"])
    
       


def syncClock():
    global clock
    if(clock != None):
        logger.info('sync with clock set to: %s' % (clock.ip))
        result = subprocess.run(["sudo", "sntp", "-sS", str(clock.ip)], capture_output=True)
        logger.info(result)




############STATE5: seance
def sendNewSeance():
    
    with _globals.lock:
        delayStart = random.randint(2, 5) * 10
        duration = random.randint(2, 4)
        _globals.startTime = datetime.datetime.now()+datetime.timedelta(seconds=delayStart)
        _globals.endTime = _globals.startTime+datetime.timedelta(minutes=duration)
    
        _globals.talker.talk(["commenconsdans", delayStart, "secondes", "terminonsdans", duration, "minute"])
        _globals.server.send_msg("timing", {
            'startTime':_globals.startTime.strftime("%d-%b-%Y (%H:%M:%S.%f)"), 
            'endTime':_globals.endTime.strftime("%d-%b-%Y (%H:%M:%S.%f)"),
            'delayStart':delayStart,
            'duration':duration
        })
    
           
def newSeanceIsReceived():
    global clock
    if clock.startTime != None and clock.endTime != None:
        _globals.startTime = clock.startTime
        _globals.endTime = clock.endTime
        return True
    return False



doPlay = False
def planSeance():
    global doPlay
    logger.info("we are going to launch the seance")
    logger.info(f"launch planned at { _globals.startTime.strftime('%d-%b-%Y (%H:%M:%S.%f)') }")
    logger.info(f"launch in {(_globals.startTime - datetime.datetime.now()).total_seconds()} seconds")

    startTimer = threading.Timer((_globals.startTime - datetime.datetime.now()).total_seconds(), playSeance)
            
    endTimer = threading.Timer((_globals.endTime - datetime.datetime.now()).total_seconds(), stopSeance, [startTimer])
    startTimer.start()
    endTimer.start()    
    doPlay = True 


def playSeance():
    global doPlay
    while doPlay:
        _globals.talker.perform()
    
    time.sleep(5)
    with _globals.lock:
        _globals.talker.talk(["recommencons"])
        _globals.internalState = 2 

def stopSeance(startTimer):
    global doPlay
    startTimer.cancel()
    if clock != None: 
        clock.startTime = None
        clock.endTime = None
    doPlay = False

    

##############SWITCH state!
def checkAndUpdateNetworkState():
    if _globals.internalState == 0:
        _globals.networkState = 0
        return

    logger.debug(f"internal state :{_globals.internalState}")
    logger.debug(f"network state :{_globals.networkState}")
    global clients
    synchro = True
    
    if len(clients) < 3:
        logger.debug("we cannot check the state of clients: everybody is not there")
        return

    for c in clients:
        logger.debug(f"internal state of {c.ip}: {c.state}")
        
        if c.status == 'up' and _globals.internalState != c.state:
            
            c.askServerState() 
            if c.state != 0 and c.state < _globals.internalState:
                synchro = False
            
    
    

    if synchro:
            _globals.networkState = _globals.internalState
    
        

def updateVolume():
    
    now = datetime.datetime.now()
    m = alsaaudio.Mixer('Digital', cardindex=0)
    '''scanCards = alsaaudio.cards()
    logger.debug(f"cards:{*scanCards,}")
    for card in scanCards:
        scanMixers = alsaaudio.mixers(scanCards.index(card))
        logger.debug(f"mixers:{*scanMixers,}")
    '''
    if now.hour > config.TIME_NIGHT or now.hour < config.TIME_DAY:
        m.setvolume(config.MIN_VOLUME)
        logger.debug(f"setting volume to {config.MIN_VOLUME}")
    else:
        m.setvolume(config.MAX_VOLUME)
        logger.debug(f"setting volume to {config.MAX_VOLUME}")




def checkStatus():
    now = datetime.datetime.now()
    delaySinceLastSpeak = now - _globals.talker.lastSpeak
    if delaySinceLastSpeak.total_seconds() > config.MAX_TIME_BETWEEN_SPEAK:
        _globals.talker.lastSpeak = now
        logger.debug("i didn't say anything for 5 minutes: restarting")
        _globals.server.stop()
        stopClients()
        _globals.internalState = 0
        

def stopClients():
    global clients
    for c in clients:
        c.disconnect()

    clients = []
            

###MAIN INIT
#0: initialize talker
updateVolume()

with _globals.lock:
    _globals.talker = Talker()
    _globals.talker.talk(["jedemarre"])

#1: get ip and hostname
nodeInfo = getHostNameIP()
_globals.ip = nodeInfo['ip']
_globals.hostname = nodeInfo['hostname']

with _globals.lock:
    _globals.talker.talk(["jaireculidentification", _globals.ip])





_globals.startTime = None
_globals.endTime = None
_globals.server = None

clients = []

try:
    while True:
        
        checkStatus()#avoids the program to be stuck in one state for too long 

        #SWITCH STATE
        if _globals.networkState != _globals.internalState:
            ##check network state
            checkAndUpdateNetworkState()
            time.sleep(2)
            continue

        #STATE 0: init connections
        if _globals.networkState == 0:
            logger.info("network state: init")
            updateVolume()
            doPlay = False #when we reach this step, stop performance if running
            stopClients()

            if _globals.server != None:
                _globals.server.stop()
            
            
            _globals.server = Server()

            _globals.server.start()

           

            with _globals.lock:
                _globals.talker.talk(["jecoute"])

            #we wait for the server to start (TODO: detect when the server has started)
            time.sleep(4)

            with _globals.lock:
                _globals.talker.talk(["jepeuxparler"])
                
            _globals.internalState = 1
            _globals.networkState = 1 #we need to go to state 1 to look for clients
            ###ici un délai extra pour laisser le temps à tout le monde d'arriver à ce stade!
            logger.debug("extra time to avoid confusion")
            time.sleep(30)


        #STATE 1: connections
        if _globals.networkState == 1:
            _globals.restarting = False
            updateVolume()
            logger.info("network state: looking for peers")
            searchFinished = lookForServers()
            if searchFinished:
                if _globals.internalState == 1:#ça a pu changer entretemps
                        _globals.internalState = 2
                
                
        #STATE 2: init clock
        if _globals.networkState == 2:
            updateVolume()
            logger.info("network state: init clock")
            _globals.clockWill = bool(random.getrandbits(1))
            
            resetClockState()
            with _globals.lock:
                if _globals.internalState == 2:#ça a pu changer entretemps
                        _globals.internalState = 3
                
        
        #STATE 3: clock negociation
        if _globals.networkState == 3:
            logger.info("network state: choose clock")
            updateVolume()
            clockState()

            if clock != None or isClock:
                with _globals.lock:
                    if not isClock:
                        _globals.talker.talk(["jerecoislheure"])
                        syncClock()
                    if _globals.internalState == 3:#ça a pu changer entretemps
                        _globals.internalState = 4
            
        #STATE 4: declaration d'intention
        if _globals.networkState == 4:
            updateVolume()
            logger.info("network state: build performance")
            _globals.talker.makePerformance()
            with _globals.lock:
                _globals.talker.talk(_globals.talker.performanceMessage) 
                if _globals.internalState == 4:#ça a pu changer entretemps
                        _globals.internalState = 5
                
                
        
        #STATE 5: planification:
        if _globals.networkState == 5:
            updateVolume()
            logger.info("network state: plan performance")
            if isClock:
                sendNewSeance()
                planSeance()
                with _globals.lock:
                    if _globals.internalState == 5:#ça a pu changer entretemps
                        _globals.internalState = 6
                   
            else:
                if newSeanceIsReceived():
                    planSeance()
                    with _globals.lock:
                        if _globals.internalState == 5:#ça a pu changer entretemps
                            _globals.internalState = 6
                        
            

        #STATE 6: seance time
        if _globals.networkState == 6:
            updateVolume()
            logger.info("network state: perform")




        #with _globals.lock:
            #server.send_msg("server_status", "test")
        
        time.sleep(5)
except KeyboardInterrupt:
    print("goodbye")

#print("ok")
