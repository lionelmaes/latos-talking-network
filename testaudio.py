from pydub import AudioSegment
from pydub.playback import play
'''
part1 = AudioSegment.from_mp3("./test-voix-sop/jessayedemeconnectera.mp3")
part2 = AudioSegment.from_mp3("./test-voix-sop/10.42.0.2.mp3")
silence = AudioSegment.silent(duration=1000)
part3 = AudioSegment.from_mp3("./test-voix-sop/10.42.0.2.mp3")
part4 = AudioSegment.from_mp3("./test-voix-sop/souhaiteraistuetrelhorloge.mp3")

sentence = part1 + part2 + silence + part3 + part4
reverse = sentence.reverse()
'''
oh = AudioSegment.from_mp3("./test-voix-sop/oh.mp3")
silence = AudioSegment.silent(duration=100)

total = oh

pitch = oh.frame_rate

# shift the pitch down by half an octave (speed will decrease proportionally)
octaves = 0.5

oaltpitch = int(oh.frame_rate * (2.0 ** octaves))

ohalt = oh._spawn(oh.raw_data, overrides={'frame_rate': oaltpitch})
ohalt = ohalt.set_frame_rate(44100)


for i in range(0, 20):
    
    total = total + silence + oh + silence + ohalt

play(total)