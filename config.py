IP_START="10.42.0.10"
IP_END="10.42.0.13"
PORT=5000
DATA_PATH = "/home/pi/latos-talking-network/data"

LOG_LEVEL = "DEBUG"
LOG_PATH = "/home/pi/latos-talking-network/logs/main.log"
#LOG_PATH = "/home/drem/Documents/WORK/latos-talking-network/logs/main.log"

#MAX_VOLUME = 85

MAX_VOLUME = 90#vernissage time
MIN_VOLUME = 1
#MAX_VOLUME = 1#test time

TIME_NIGHT = 21
TIME_DAY = 9

MAX_FRUSTRATION = 5
MAX_TIME_BETWEEN_SPEAK = 300#max 5 minutes of silence before reset