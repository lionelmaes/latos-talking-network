import logging
import config
from logging.handlers import RotatingFileHandler
import sys

formatter = logging.Formatter('%(asctime)s :: %(module)s :: %(lineno)d :: %(levelname)s :: %(message)s')
logger = logging.getLogger('latos-logger')
logger.setLevel(config.LOG_LEVEL)

#log to file
logHandler = RotatingFileHandler(config.LOG_PATH, 'a', 1000000, 1)
logHandler.setLevel(config.LOG_LEVEL)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)

#also log to stdout
streamHandler = logging.StreamHandler()
streamHandler.setLevel(config.LOG_LEVEL)
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)