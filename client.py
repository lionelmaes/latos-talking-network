
from logger import logger
import socketio
import _globals
import random
import datetime

class Client:
    

    def __init__(self, ip, port):
        self.ip = ip
        self.state = 0
        self.clockHistory = 0
        self.clockWill = None
        self.clockAuthorization = None
        self.clockAuths = None
        self.sio = socketio.Client()
        self.port = port
        self.status = 'down'
        self.restarting = False
        self.startTime = None
        self.endTime = None

       
        @self.sio.on('server_state')
        def on_server_state(args):
            
            logger.debug(f"got server state: {args}")
        
            self.state = args
        
        @self.sio.on('server_stop')
        def on_server_stop():
            self.restart()

        @self.sio.on('server_clockwill')
        def on_server_clockwill(args):
            
            if self.clockWill != None:#on a déjà la réponse! (mais pourquoi est-ce que ça se déclenche plusieurs fois bordel?)
                return
            self.clockWill = args
            
            with _globals.lock:
                if self.clockWill:
                    
                    _globals.talker.talk(["jentendsque", self.ip, "veutetrelhorloge"])
                else:
                     
                    _globals.talker.talk(["jentendsque", self.ip, "neveutpasetrelhorloge"])
            
            
            chances = 0.5 - self.clockHistory * 0.2
            randomVal = random.random()
            logger.debug(f"{self.ip}, tu as {chances * 100} % de chances que j'accepte que tu sois l'horloge")
            self.clockAuthorization = randomVal < chances
            logger.debug(f"{self.ip}, voici ma décision {self.clockAuthorization}")
            with _globals.lock:
                
                if self.clockWill and not self.clockAuthorization: 
                    _globals.talker.talk(["jeneveuxpasque", self.ip, "soitlhorloge"])
                elif self.clockWill:
                    _globals.talker.talk(["jaccepteque", self.ip, "soitlhorloge"])

                self.sendClockAuthorization()


        @self.sio.on('server_clock_auths')
        def on_server_clock_auths(data):
            logger.debug(f"{self.ip} got this table auth {*data,}")
            self.clockAuths = data
           

        @self.sio.on('connect')
        def on_connect():
            if self.status == 'up':
                return#déjà fait

            self.status = 'up'
            with _globals.lock:
                _globals.talker.talk(["jaitrouve", self.ip])
                
                
        
        @self.sio.on('disconnect')
        def on_disconnect():
            self.status = 'down'
                
                
                
        @self.sio.on('timing')
        def on_timing(data):
           
            self.startTime = datetime.datetime.strptime(data['startTime'], "%d-%b-%Y (%H:%M:%S.%f)")
            self.endTime = datetime.datetime.strptime(data['endTime'], "%d-%b-%Y (%H:%M:%S.%f)")



    def restart(self):
        if _globals.restarting:
            logger.debug("already restarting")
            return
        _globals.restarting = True
        self.disconnect()
        self.status = 'down'
        _globals.server.stop()
        _globals.internalState = 0
        logger.debug(f"disconnected from {self.ip}")
        with _globals.lock:
            _globals.talker.talk([self.ip, "nenousecouteplus"]) 
            _globals.talker.talk(["jeredemarre"])
    
    def askServerState(self):
        
        self.sio.emit('ask_server_state')
    
    def askServerClockWill(self):
        
        with _globals.lock:
           
            _globals.talker.talk([self.ip, "veuxtuetrelhorloge"])
        self.sio.emit('ask_server_clockwill', {'ip':_globals.ip})

    def sendClockAuthorization(self):
        self.sio.emit('clock_authorization', {'ip':_globals.ip, 'auth':self.clockAuthorization})


    def disconnect(self):
        self.sio.disconnect()
        
    def connect(self):
        with _globals.lock:
            _globals.talker.talk(["jecherche", self.ip])
        #print("trying to connect to http://%s:%s" % (self.serverIP, self.port))
        
        try:
            #logger.debug('connection attempt!')
            self.sio.connect('http://%s:%s' % (self.ip, self.port))
            self.sio.emit('node_connection', {'ip':_globals.ip})
            
            return True
        except:
            return False
            