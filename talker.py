import time
from pydub import AudioSegment
from pydub.playback import play
import random
from logger import logger
import config 
import datetime


class Talker:

    def __init__(self):
        print('new talker')
        self.performance = None
        self.performanceMessage = ""
        self.lastSpeak = datetime.datetime.now()

    

    def makePerformance(self):
        self.makePerformance3()


    def makePerformance3(self):

        elements = ["ah", "oh", "silence", "ts", "fr", 
            "pa", "pe", "pi", "po", "pu", "pou",
            "ta", "te", "ti", "to", "tu", "tou",
            "ma", "me", "mi", "mo", "mu", "mou",
            "ga", "ge", "gi", "go", "gu", "gou",
            "sa", "se", "si", "so", "su", "sou"
        ]
        nuances = ["court", "long"]
        self.performanceMessage = ["jeperformerai"]
        self.performance = AudioSegment.empty()

        parts = random.randint(2, 4)
        lastPart = None
        i = 0
        
        while i < parts:
            
        
            nb = random.randint(1,10)
            element = random.choice(elements)
            nuance = random.choice(nuances)
            
            if lastPart != None and lastPart["element"] == element:
                if element == "silence" or (nuance == lastPart["nuance"]):
                    i -= 1
                    print("non")
                    continue

            lastPart = {"element":element, "nuance":nuance}
            
            self.performanceMessage.extend([nb, element, nuance])
            if element == "silence":
                if nuance == "long":
                    audioPart = AudioSegment.silent(duration=500)
                else:
                   audioPart = AudioSegment.silent(duration=250)
            else:
                audioPart = AudioSegment.from_mp3(f"{config.DATA_PATH}/{element}{nuance}.mp3")
            for j in range(0, nb):
                self.performance = self.performance + audioPart
        
            i += 1

    def makePerformance2(self):
        elements = ["ah", "oh", "silence"]
        nuances = ["court", "long"]
        self.performanceMessage = ["jeperformerai"]
        self.performance = AudioSegment.empty()

        parts = random.randint(2, 8)
        lastPart = None
        i = 0
        
        while i < parts:
            
            element = random.choice(elements)
            nuance = random.choice(nuances)
            
            if lastPart != None and lastPart["element"] == element:
                if element == "silence" or (nuance == lastPart["nuance"]):
                    i -= 1
                    continue

            lastPart = {"element":element, "nuance":nuance}
            
            self.performanceMessage.extend(["1", element, nuance])
            if element == "silence":
                if nuance == "long":
                    audioPart = AudioSegment.silent(duration=500)
                else:
                   audioPart = AudioSegment.silent(duration=250)
            else:
                audioPart = AudioSegment.from_mp3(f"{config.DATA_PATH}/{element}{nuance}.mp3")
            
            self.performance = self.performance + audioPart
        
            i += 1
           
    
    def makePerformance1(self):
        elements = ["ah", "oh", "silence"]
        nuances = ["court", "long"]
        self.performanceMessage = ["jeperformerai"]
        self.performance = AudioSegment.empty()

        parts = random.randint(2, 4)
        lastPart = None
        i = 0
        
        while i < parts:
            
        
            nb = random.randint(1,10)
            element = random.choice(elements)
            nuance = random.choice(nuances)
            
            if lastPart != None and lastPart["element"] == element:
                if element == "silence" or (nuance == lastPart["nuance"]):
                    i -= 1
                    
                    continue

            lastPart = {"element":element, "nuance":nuance}
            
            self.performanceMessage.extend([nb, element, nuance])
            if element == "silence":
                if nuance == "long":
                    audioPart = AudioSegment.silent(duration=500)
                else:
                   audioPart = AudioSegment.silent(duration=250)
            else:
                audioPart = AudioSegment.from_mp3(f"{config.DATA_PATH}/{element}{nuance}.mp3")
            for j in range(0, nb):
                self.performance = self.performance + audioPart
        
            i += 1
            
        
        
    def perform(self):
        if self.performance == None:
            return
        self.lastSpeak = datetime.datetime.now()
        play(self.performance)       


    def talk(self, message):
        output = AudioSegment.empty()
        logger.info(message)
        for segment in message:
            
            audioseg = AudioSegment.from_mp3(f"{config.DATA_PATH}/{segment}.mp3")
            output = output + audioseg
        self.lastSpeak = datetime.datetime.now()
        play(output)


if __name__ == "__main__":
    talker = Talker()

    talker.makePerformance()
    talker.talk(talker.performanceMessage)
    for i in range(0, 5):
        talker.perform()
